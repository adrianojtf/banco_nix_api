# Banco_Nix_API

Projeto visa atender por meio de API Rest, o gerenciamento externo de transferências do banco Nix(fictício). de transferências, permitindo a inserção, listagem e exclusão.

## Instalação e inicialização:

Para instalação do Web Service disponibilizamos dois meios para tal, são elas:

### via Docker:

```
# Baixar o projeto:
git clone https://gitlab.com/adrianojtf/banco_nix_api.git

# Entrar no diretório dele:
cd banco_nix_api

# Criar a imagem do container:
sudo docker build -t banconix_ws .

# Verificar o IMAGE ID:
docker images

# Rodar o contaneir:
docker run "IMAGE ID"

```

### via Virtual env:

```
# Baixar o projeto:
git clone https://gitlab.com/adrianojtf/banco_nix_api.git

# Entrar no diretório dele:
cd banco_nix_api

# Acessar ambiente virtual
source env-python3.7/bin/activate

# Rodar o WS
python api/app.py

```

## Acesso a documentação via Swagger:

### via Docker:

```
# Após rodar o docker pegar o endereço IP na variavel "O URL de acesso:", EX:

$ docker run be01d134d26f
    O URL de acesso: 172.17.0.2 :5000
    * Serving Flask app "app" (lazy loading)
    * Environment: production

# No navegador inserir a URL:

http://"O URL de acesso":5000/

```

### via Virtual env:

```
http://127.0.0.1:5000/

```