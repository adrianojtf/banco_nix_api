from flask_restplus import Resource, reqparse, Api, Namespace
from models.usuario import Usuario
from restplus import api


atributos = reqparse.RequestParser()
atributos.add_argument('nome', type=str, required=True, help="The field 'nome' cannot be left blank.")
atributos.add_argument('cnpj', type=str, required=True, help="The field 'senha' cannot be left blank.")


class User(Resource):

    
    def get(self, user_id):
        user = Usuario.find_usuario(user_id)
        if user:
            return user.json()
        return {'message': 'User not found.'}, 404

    def delete(self, user_id):
        user = Usuario.find_usuario(user_id)
        if user:
            try:
                user.delete_usuario()
            except:
                return {'message': 'An internal error ocurred trying to delete'}, 500
            return user.json(), 200

        return {'message': 'User not found.'}, 404

@api.route('/usuario/cadastro')
class UserRegister(Resource):
    @api.doc(params={'nome': 'An Name', 'cnpj':'An cnpj'})
    def post(self):
        dados = atributos.parse_args()
        if Usuario.find_by_nome(dados['nome']):
            return {"message": "The name '{}' already exists ".format(dados['nome'])}
        user = Usuario(**dados)
        user.save_usuario()
        return {"message": "User created successfully "}, 201

