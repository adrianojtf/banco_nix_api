from flask_restplus import Resource, reqparse
from models.transferencia import TransferenciaModel
from datetime import datetime
from restplus import api, ns


class Transferencias(Resource):
    
    filtros = reqparse.RequestParser()
    filtros.add_argument('data_min', type=str, help='Data Inicial - EX: 2020-02-18 21:00:00')
    filtros.add_argument('data_max', type=str, help='Data Final - EX: 2020-02-18 21:30:00')
    filtros.add_argument('pagador_nome', type=str, help='Nome do transferidor')
    filtros.add_argument('beneficiario_nome', type=str, help='Nome do beneficiário')
    filtros.add_argument('limit', type=int, required=True, help='Limite de resultados exibidos')
    filtros.add_argument('offset', type=int, required=True, help='Usado para paginação, \
                                                                  o valor inserido será o \
                                                                      inicio da leitura.')

    @api.response(404, 'No search results')
    @api.response(200, 'Success')
    @api.expect(filtros)
    def get(self, usuario_id):
        dados = Transferencias.filtros.parse_args()
        base = 'api/banco.db'

        transfs = TransferenciaModel.find_byFilters(base, usuario_id, **dados)

        if transfs:
            transfers = []
            for transf in transfs:
                transfers.append({
                    'id': transf[0],
                    'usuario_id': transf[1],
                    'pagador_nome': transf[2],
                    'pagador_banco': transf[3],
                    'pagador_agencia': transf[4],
                    'pagador_conta': transf[5],
                    'beneficiario_nome': transf[6],
                    'beneficiario_banco': transf[7],
                    'beneficiario_agencia': transf[8],
                    'beneficiario_conta': transf[9],
                    'valor': transf[10],
                    'tipo': transf[11],
                    'data_transação': transf[12],
                    'status': transf[13]
                })
            return {'Tranferencias': transfers}, 200

        return {'message': 'No search results.'}, 404


class Transferencia(Resource):

    @api.response(404, 'Transfer not found')
    @api.response(200, 'Success')
    def get(self, id):
        transf = TransferenciaModel.find_transferencia(id)
        if transf:
            return transf.json(), 200
        return {'message': 'Transfer not found.'}, 404

    @api.response(404, 'Transfer not found')
    @api.response(200, 'Success')
    @api.response(500, 'An internal error ocurred trying to delete')
    def delete(self, id):
        transf = TransferenciaModel.find_transferencia(id)
        if transf:
            try:
                transf.delete_transferencia()
            except:
                return {'message': 'An internal error ocurred trying to delete'}, 500
            return transf.json(), 200
        return {'message': 'Transfer not found.'}, 404


class TranferRegister(Resource):

    argumentos = reqparse.RequestParser()
    argumentos.add_argument('pagador_nome', type=str, help='Nome do transferidor')
    argumentos.add_argument('pagador_banco', type=str, required=True, help='Banco do transferidor')
    argumentos.add_argument('pagador_agencia', type=str, required=True, help='Agência do transferidor')
    argumentos.add_argument('pagador_conta', type=str, required=True, help='Conta do transferidor')
    argumentos.add_argument('beneficiario_nome', type=str, help='Nome do beneficiário')
    argumentos.add_argument('beneficiario_banco', type=str, required=True, help='Banco do beneficiário')
    argumentos.add_argument('beneficiario_agencia', type=str, required=True, help='Agência do beneficiário')
    argumentos.add_argument('beneficiario_conta', type=str, required=True, help='Conta do beneficiário')
    argumentos.add_argument('valor', type=float, required=True, help='Valor a ser transferido')

    @api.response(200, 'Success')
    @api.response(500, 'An internal error ocurred trying to delete')
    @api.expect(argumentos)
    def post(self, usuario_id):
        data = datetime.now()
        dados = TranferRegister.argumentos.parse_args()

        if dados['pagador_banco'].upper() == dados['beneficiario_banco'].upper():
            tipo = 'CC'
        elif (data.hour >= 10 and data.hour < 16) and dados['valor'] <= 5000:
            tipo = 'TED'
        else:
            tipo = 'DOC'    

        if dados['valor'] <= 100000:
            status = 'OK'
        else:
            status = 'ERRO'

        transf = TransferenciaModel(usuario_id, tipo, data, status, **dados)

        try:
            transf.save_transferencia()
        except:
            return {'message': 'An internal error ocurred trying to save'}, 500
            
        return transf.json(), 200