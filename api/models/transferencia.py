from sql_alchemy import banco
from models.usuario import Usuario
from datetime import datetime
import sqlite3


class TransferenciaModel(banco.Model):
    __tablename__ = 'transferencias'

    id = banco.Column(banco.Integer, primary_key=True)
    usuario_id = banco.Column(banco.Integer, banco.ForeignKey("usuarios.id"), nullable=False)
    pagador_nome = banco.Column(banco.String(128))
    pagador_banco = banco.Column(banco.String(3), nullable=False)
    pagador_agencia = banco.Column(banco.String(4), nullable=False)
    pagador_conta = banco.Column(banco.String(6), nullable=False)
    beneficiario_nome = banco.Column(banco.String(128))
    beneficiario_banco = banco.Column(banco.String(3), nullable=False)
    beneficiario_agencia = banco.Column(banco.String(4), nullable=False)
    beneficiario_conta = banco.Column(banco.String(6), nullable=False)
    valor = banco.Column(banco.Float(precision=2, decimal_return_scale=15), nullable=False)
    tipo = banco.Column(banco.String(3), nullable=False)
    data_transação = banco.Column(banco.DateTime(), nullable=False)
    status = banco.Column(banco.String(2), nullable=False)

    def __init__(self, usuario_id, tipo, data, status, **kwargs):
        self.usuario_id = usuario_id
        self.pagador_nome = kwargs.get('pagador_nome')
        self.pagador_banco = kwargs.get('pagador_banco')
        self.pagador_agencia = kwargs.get('pagador_agencia')
        self.pagador_conta = kwargs.get('pagador_conta')
        self.beneficiario_nome = kwargs.get('beneficiario_nome')
        self.beneficiario_banco = kwargs.get('beneficiario_banco')
        self.beneficiario_agencia = kwargs.get('beneficiario_agencia')
        self.beneficiario_conta = kwargs.get('beneficiario_conta')
        self.valor = kwargs.get('valor')
        self.tipo = tipo
        self.data_transação = data
        self.status = status

    def json(self):
        return {
            'id': self.id,
            'usuario_id': self.usuario_id,
            'pagador_nome': self.pagador_nome,
            'pagador_banco': self.pagador_banco,
            'pagador_agencia': self.pagador_agencia,
            'pagador_conta': self.pagador_conta,
            'beneficiario_nome': self.beneficiario_nome,
            'beneficiario_banco': self.beneficiario_banco,
            'beneficiario_agencia': self.beneficiario_agencia,
            'beneficiario_conta': self.beneficiario_conta,
            'valor': self.valor,
            'tipo': self.tipo,
            'data_transação': self.data_transação.isoformat(' ', timespec='seconds'),
            'status': self.status
        }

    @classmethod
    def find_transferencia(cls, id):
        transf = cls.query.filter_by(id=id).first()
        if transf:
            return transf
        return None

    def save_transferencia(self):
        banco.session.add(self)
        banco.session.commit()

    def delete_transferencia(self):
        banco.session.delete(self)
        banco.session.commit()

    @staticmethod
    def find_byFilters(base, usuario_id, **filtros):
        connection = sqlite3.connect(base)
        cursor = connection.cursor()

        query = "SELECT * FROM transferencias AS t WHERE usuario_id = {}".format(usuario_id)
        if filtros['data_min'] and filtros['data_max']:
            query += " AND t.data_transação between '{}' and '{}'".format(filtros['data_min'], filtros['data_max'])
        if filtros['pagador_nome']:
            query += " AND t.pagador_nome='{}'".format(filtros['pagador_nome'])
        if filtros['beneficiario_nome']:
            query += " AND t.beneficiario_nome='{}'".format(filtros['beneficiario_nome'])
        query += " limit {} offset {}".format(filtros['limit'], filtros['offset'])
        print(query)
        cursor.execute(query)
        records = cursor.fetchall()
        
        return records

    @classmethod
    def find_byUserId(cls, usuario_id):
        transf = cls.query.filter_by(usuario_id=usuario_id)
        if transf:
            return transf
        return None    
 