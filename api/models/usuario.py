from sql_alchemy import banco


class Usuario(banco.Model):
    __tablename__ = 'usuarios'

    id = banco.Column(banco.Integer, primary_key=True)
    nome = banco.Column(banco.String(128))
    cnpj = banco.Column(banco.String(14))

    def __init__(self, nome, cnpj):
        self.nome = nome
        self.cnpj = cnpj

    def json(self):
        return {
            'id': self.id,
            'nome': self.nome
        }

    @classmethod
    def find_usuario(cls, id):
        user = cls.query.filter_by(id=id).first()
        if user:
            return user
        return None

    def save_usuario(self):
        banco.session.add(self)
        banco.session.commit()

    def delete_usuario(self):
        banco.session.delete(self)
        banco.session.commit()
        
    @classmethod
    def find_by_nome(cls, nome):
        user = cls.query.filter_by(nome=nome).first()
        if user:
            return user
        return None