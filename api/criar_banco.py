import sqlite3

connection = sqlite3.connect('banco.db')
cursor = connection.cursor()

cria_tabela = "CREATE TABLE IF NOT EXISTS hoteis (hotel_id text primary key, nome text,\
    estrelas real, diaria real, cidade text)"

create_hotel = "INSERT INTO hoteis VALUES('alfa', 'Hotel Alfa', 4.0, 370.00, 'Rio de Janeiro')"
create_hotel2 = "INSERT INTO hoteis VALUES('beta', 'Hotel Beta', 4.8, 470.00, 'Rio de Janeiro')"
create_hotel3 = "INSERT INTO hoteis VALUES('charles', 'Hotel Charles', 3.0, 170.00, 'Rio de Janeiro')"
create_hotel4 = "INSERT INTO hoteis VALUES('delta', 'Hotel Delta', 5.0, 870.00, 'Rio de Janeiro')"

cursor.execute(cria_tabela)
cursor.execute(create_hotel)
connection.commit()
connection.close()
