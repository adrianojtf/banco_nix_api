from flask_restplus import Api

api = Api(default='Transfer', default_label = 'teste', version='1.0', title='BancoNix_API',
    description='Nix bank transfer management API.'
)

ns = api.namespace('Transfer', description='Banco Nix Transfer')