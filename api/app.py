from flask import Flask
from flask_restplus import Api
from resources.transferencia import Transferencias, Transferencia, TranferRegister
from resources.usuario import User, UserRegister
import subprocess


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///banco.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['JWT_SECRET_KEY'] = 'DontTellAnyone'
api = Api(app, default='Transferência', \
          default_label = 'Gerenciamento de transações.', \
          version='1.0', title='BancoNix_API',
          description='O objetivo desta API é gerenciar externamente as \
          transferências, permitindo a inserção, listagem e exclusão.')

@app.before_first_request
def cria_banco():
    banco.create_all()

api.add_resource(Transferencias, '/transferencias/<int:usuario_id>')
api.add_resource(Transferencia, '/transferencia/<int:id>')
#api.add_resource(User, '/usuario/<int:usuario_id>')
#api.add_resource(UserRegister, '/usuario/cadastro')
api.add_resource(TranferRegister, '/transferencia/cadastro/<int:usuario_id>')

if __name__ == '__main__':
    from sql_alchemy import banco
    host_ip = subprocess.getoutput('hostname -I')
    print("O URL de acesso: {}:5000".format(host_ip))
    banco.init_app(app)
    app.run(debug=True, host='0.0.0.0')
