FROM centos:7

WORKDIR /usr/src/app

USER root

RUN yum -y install epel-release \
    python-pip httpd mod_wsgi yum clean all \
    python36 python36-libs python36-devel python36-pip \ 
    python3-devel build-essential libssl-devel libffi-devel \ 
    libxml2-devel libxslt1-devel zlib1g-devel; \
    yum groupinstall -y "Development Tools"; \
    
RUN pip3.6 install pep8; \
    pip3.6 install --upgrade autopep8; \
    pip3.6 install pylint

COPY requirements.txt ./

RUN pip3.6 install --no-cache-dir -r requirements.txt

COPY ./api ./api

CMD [ "python3.6", "./api/app.py" ]
